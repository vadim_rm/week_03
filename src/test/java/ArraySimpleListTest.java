package ru.edu;


import org.junit.Test;


import java.util.List;


import static org.junit.Assert.*;


public class ArraySimpleListTest {

    @Test

    public void add() {

        ArraySimpleList<Integer> list = new ArraySimpleList<Integer>();

        list.add(1);

        list.add(2);

        list.add(3);

        assertEquals(3, list.size());

        assertEquals(new Integer(1), list.get(0));

        assertEquals(new Integer(2), list.get(1));

        assertEquals(new Integer(3), list.get(2));

        assertNull(list.get(3));

    }

    @Test

    public void set() {

        ArraySimpleList<Integer> list = new ArraySimpleList<>();

        list.add(1);

        list.add(2);

        list.add(3);

        assertEquals(new Integer(2), list.get(1));

        list.set(1, 10);

        assertEquals(new Integer(10), list.get(1));

    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)

    public void setNotInList() {

        ArraySimpleList<Integer> list = new ArraySimpleList<>();

        list.set(1, 10);

    }

    @Test

    public void get() {

        ArraySimpleList<Integer> list=new ArraySimpleList<>();

        list.add(2);

        list.add(4);

        list.add(8);

        list.add(16);

        assertEquals(4,list.size());

        assertEquals(new Integer(8), list.get(2));

        assertNull(list.get(4));

        assertNotNull(list.get(1));

    }

    @Test

    public void remove() {

        ArraySimpleList<Integer> list = new ArraySimpleList<>();

        list.add(1);

        list.add(2);

        list.add(3);

        list.add(4);

        list.add(5);

        assertEquals(5, list.size());

        list.remove(3);

        assertEquals(4, list.size());

        assertEquals(new Integer(5), list.get(3));

        list.remove(3);

        assertEquals(new Integer(3), list.get(2));

        list.remove(0);

        assertEquals(2, list.size());

        assertEquals(new Integer(2), list.get(0));

    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)

    public void removeNotInList() {
        ArraySimpleList<Integer> list = new ArraySimpleList<>();
        list.remove(0);
    }

    @Test

    public void indexOf() {

        ArraySimpleList<String> list=new ArraySimpleList<>();

        list.add("hello");

        list.add("Guten tag");

        list.add("Olla");

        assertEquals(0, list.indexOf("hello"));

        assertEquals(1, list.indexOf("Guten tag"));

        assertEquals(2,list.indexOf("Olla"));

        assertEquals(-1, list.indexOf("Bonjour"));

    }

    @Test

    public void size() {

        ArraySimpleList<Integer> list=new ArraySimpleList<>();

        assertEquals(0, list.size());

        list.add(2);

        assertEquals(1, list.size());

        list.add(4);

        assertEquals(2, list.size());
    }
}