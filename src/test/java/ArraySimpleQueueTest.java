package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {

    @Test

    public void offer() {
        ArraySimpleQueue<Integer> queue = new ArraySimpleQueue<>(1);

        assertEquals(true, queue.offer(1));

        assertEquals(false, queue.offer(2));
    }


    @Test

    public void poll() {
        ArraySimpleQueue<Integer> queue = new ArraySimpleQueue<>(2);

        assertEquals(null, queue.poll());

        queue.offer(1);

        queue.offer(2);

        assertEquals(new Integer(1), queue.poll());

        assertEquals(new Integer(2), queue.poll());

        assertEquals(0, queue.size());
    }


    @Test
    public void peek() {

        ArraySimpleQueue<Integer> queue = new ArraySimpleQueue<>(3);

        assertEquals(null, queue.peek());

        queue.offer(1);

        queue.offer(2);

        queue.offer(3);

        assertEquals(new Integer(1), queue.peek());

        assertEquals(new Integer(1), queue.peek());
    }


    @Test

    public void size() {

        ArraySimpleQueue<Integer> queue = new ArraySimpleQueue<>(5);

        assertEquals(0, queue.size());
    }


    @Test

    public void capacity() {

        ArraySimpleQueue<Integer> queue = new ArraySimpleQueue<>(5);

        assertEquals(5, queue.capacity());

    }
}