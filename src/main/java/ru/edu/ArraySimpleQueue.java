package ru.edu;


public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * хранилище для очереди.
     */

    private ArraySimpleList<T> list = new ArraySimpleList<>();

    /**
     * максимальная вместимость очереди.
     */

    private int capacity = 0;


    /**
     * конструктор.
     * @param cap
     */

    public ArraySimpleQueue(final int cap) {

        capacity = cap;

    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */

    @Override

    public boolean offer(final T value) {

        if (size() == capacity) {

            return false;

        }

        list.add(value);

        return true;

    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */

    @Override

    public T poll() {

        if (size() == 0) {

            return null;

        }

        T value = list.get(0);

        list.remove(0);

        return value;

    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */

    @Override

    public T peek() {

        if (size() == 0) {

            return null;

        }

        return list.get(0);

    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */

    @Override

    public int size() {

        return list.size();

    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */

    @Override

    public int capacity() {

        return capacity;
    }

}