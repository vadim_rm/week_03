package ru.edu;

public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     ** задаем начальное значение размера массива.
     */

    private final int defaultCapacity = 1;

    /**
     * обьявляем массив.
     */

    private T[] array;

    /**
     * обьявляем переменную размера списка.
     */

    private int size = 0;


    /**
     * создание конструктора.
     */

    @SuppressWarnings("unchecked")

    public ArraySimpleList() {

        this.array = (T[]) new Object[defaultCapacity];

    }

    /**
     *метод для удаления элемента по индексу.
     * @param element

     */

    private void removeElement(final int element) {

        for (int i = element + 1; i < size; ++i) {

            array[i - 1] = array[i];

        }

        array[--size] = null;

    }

    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */

    @Override

    @SuppressWarnings("unchecked")

    public void add(final T value) {

        if (size >= array.length) {

            T[] array1 = (T[]) new Object[array.length * 2];

            for (int i = 0; i < size; ++i) {

                array1[i] = array[i];

            }

            array = array1;

        }

        array[size] = value;

        ++size;

    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */

    @Override

    public void set(final int index, final T value) {

        if (index >= size) {

            throw new ArrayIndexOutOfBoundsException();

        }

        array[index] = value;

    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */

    @Override

    public T get(final int index) {

        if (index >= size) {

            return null;

        }

        return (T) array[index];

    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */

    @Override

    public void remove(final int index) {

        if (index >= size) {

            throw new ArrayIndexOutOfBoundsException();

        }

        removeElement(index);

    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */

    @Override

    public int indexOf(final T value) {

        for (int i = 0; i < size; i++) {

            if (array[i].equals(value)) {

                return i;

            }

        }

        return -1;

    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */

    @Override

    public int size() {

        return size;

    }

}